from django.urls import path
from employees import views

app_name = "api"

urlpatterns = [
    path("subdivisions/", views.SubdivisionListAPIView.as_view(), name="api_subdivisions"),
    path('users/', views.UserAPIView.as_view(), name="api_users"),
    path('profiles/', views.ProfileAPIView.as_view(), name="api_profiles"),
    path('positions/', views.PositionAPIView.as_view(), name="api_positions"),
    path('organizations/', views.OrganizationAPIView.as_view(), name="api_organizations")
]
