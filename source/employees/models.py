from django.contrib.auth import get_user_model
from django.db import models


class Profile(models.Model):
    subdivision = models.ForeignKey("Subdivision", on_delete=models.CASCADE, null=True, blank=True,
                                 related_name="employees", verbose_name="Подразделение")
    boss = models.ForeignKey("Position", on_delete=models.CASCADE, null=True, blank=True,
                                 related_name="subordinates", verbose_name="Начальник")
    user = models.OneToOneField(get_user_model(), related_name="profile", on_delete=models.CASCADE, verbose_name="Пользователь")
    patronymic = models.CharField(max_length=100, null=True, blank=True, verbose_name="Отчество")
    birth_date = models.DateField(null=True, blank=True, verbose_name="Дата рождения")
    avatar = models.ImageField(upload_to="user_pics", null=True, blank=True, verbose_name="Аватар")
    phone_number = models.CharField(max_length=150, null=True, blank=True, verbose_name="Контактный номер телефона")
    code = models.CharField(max_length=100, null=True, blank=True, verbose_name="Код")

    def __str__(self):
        return f'{self.user.last_name} {self.user.first_name}'


class Subdivision(models.Model):
    name = models.CharField(max_length=150, verbose_name="Наименование подразделения")
    organization = models.ForeignKey("Organization", on_delete=models.CASCADE,
                                     related_name="divisions", verbose_name="Организация")
    division = models.ForeignKey("Subdivision", on_delete=models.CASCADE, null=True, blank=True,
                                 related_name="division_subdivisions", verbose_name="Вышестоящий отдел")
    boss = models.ForeignKey("Position", on_delete=models.CASCADE, null=True, blank=True,
                                 related_name="position_subdivisions", verbose_name="Начальник")

    def __str__(self):
        return self.name


class Position(models.Model):
    name = models.CharField(max_length=150, verbose_name="Должность")
    subdivision = models.ForeignKey("Subdivision", on_delete=models.CASCADE,
                                    related_name="subdivision_positions", verbose_name="Отдел")
    employee = models.ForeignKey(get_user_model(), on_delete=models.CASCADE,
                                 related_name="employee_positions", verbose_name="Сотрудник")

    def __str__(self):
        return f'{self.name} {self.subdivision.name} {self.employee.last_name} {self.employee.first_name} ' \
               f'{self.employee.profile.patronymic}'


class Organization(models.Model):
    name = models.CharField(max_length=255, verbose_name="Наименование организации")

    def __str__(self):
        return self.name