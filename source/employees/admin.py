from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from employees.models import Profile, Position, Subdivision, Organization


class ProfileInline(admin.StackedInline):
    model = Profile
    fields = ["avatar", "patronymic", "birth_date", "phone_number", "code"]
    fk_name = "user"


class ProfileAdmin(UserAdmin):
    inlines = [ProfileInline]


User = get_user_model()
admin.site.unregister(User)
admin.site.register(User, ProfileAdmin)
admin.site.register(Position)
admin.site.register(Subdivision)
admin.site.register(Organization)
admin.site.register(Profile)

