from django.contrib.auth import get_user_model
from rest_framework import serializers

from employees.models import Subdivision, Position, Profile, Organization


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ["id", "first_name", "last_name"]


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = "__all__"


class DivisionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subdivision
        fields = "__all__"


class PositionSerializer(serializers.ModelSerializer):
    subdivision = DivisionSerializer(required=False)
    employee = UserSerializer(required=False)
    class Meta:
        model = Position
        fields = "__all__"


class SubdivisionSerializer(serializers.ModelSerializer):
    organization = OrganizationSerializer(required=False)
    division = DivisionSerializer(required=False)
    boss = PositionSerializer(required=False)
    class Meta:
        model = Subdivision
        fields = "__all__"


class ProfileSerializer(serializers.ModelSerializer):
    subdivision = SubdivisionSerializer(required=False)
    boss = PositionSerializer(required=False)
    class Meta:
        model = Profile
        fields = "__all__"
