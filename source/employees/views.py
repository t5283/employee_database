from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from employees.models import Subdivision, Position, Profile, Organization
from employees.serializers import SubdivisionSerializer, PositionSerializer, UserSerializer, ProfileSerializer, \
    OrganizationSerializer


class UserAPIView(APIView):
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        users = get_user_model().objects.all()
        serializer = self.serializer_class(instance=users, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class ProfileAPIView(APIView):
    serializer_class = ProfileSerializer

    def get(self, request, *args, **kwargs):
        profiles = Profile.objects.all()
        serializer = self.serializer_class(instance=profiles, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class PositionAPIView(APIView):
    serializer_class = PositionSerializer

    def get(self, request, *args, **kwargs):
        positions = Position.objects.all()
        serializer = self.serializer_class(instance=positions, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class OrganizationAPIView(APIView):
    serializer_class = OrganizationSerializer

    def get(self, request, *args, **kwargs):
        organizations = Organization.objects.all()
        serializer = self.serializer_class(instance=organizations, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class SubdivisionListAPIView(APIView):
    serializer_class = SubdivisionSerializer

    def get(self, request, *args, **kwargs):
        subdivisions = Subdivision.objects.all()
        serializer = self.serializer_class(instance=subdivisions, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)